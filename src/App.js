import { useStore, actions } from "./Store"



function App() {
    const [state, dispatch] = useStore()
    const { todos, todoInput } = state
    const handleAdd = () => {
        dispatch(actions.addTodo(todoInput))
    }

    return (
        <>
            <input
                value={todoInput}
                placeholder="Click Add Để Thêm......"
                onChange={e => {
                    dispatch(actions.setTodoInput(e.target.value))
                }}
            />
            <button onClick={handleAdd}>ADD</button>
            {todos.map((todo, index) => <li key={index}>{todo}</li>)}
        </>
    )
}



export default App