import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { Storeprovider } from './Store';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Storeprovider>
      <App />
    </Storeprovider>
  </React.StrictMode>
);
